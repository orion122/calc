def razbiene_stroki(string):
    for i in string:
        if i == '+' or i == '-' or i == '*' or i == '/':
            indeks = string.index(i)
            a = float(string[:indeks])
            b = float(string[indeks + 1:])
            operation = i
            break
    return a, b, operation


def podschet(a,b,operation):    
    if operation == '+':
        print ('=', a + b)
    elif operation == '-':
        print ('=', a - b)
    elif operation == '*':
        print ('=', a * b)
    else:
        print ('=', a / b)


print ('Выход из калькулятора - команда "exit"')
stroka = input('Введите выражение\n')
while True:
    if stroka == 'exit':
        break
    chislo_1, chislo_2, znak = razbiene_stroki(stroka)
    podschet(chislo_1, chislo_2, znak)
    stroka = input('Введите выражение\n')
